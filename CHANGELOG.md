# Changelog

0.2.0 (ksindi)
--------------

* Add Client Upload method
* MakeRequest should only return error
* Fix encoding for signature base string

0.1.0 (ksindi)
--------------

* Initial release
